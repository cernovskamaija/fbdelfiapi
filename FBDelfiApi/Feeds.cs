﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FBDelfiApi
{
    public class Feeds
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public string PublicationDate { get; set; }
    }
}