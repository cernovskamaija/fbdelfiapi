namespace FBDelfiApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateUserCustomFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Avatar", c => c.String());
            AddColumn("dbo.AspNetUsers", "DelfiChannel", c => c.String(nullable: false, defaultValue: "delfi"));
            AddColumn("dbo.AspNetUsers", "NumberOfRecords", c => c.Int(nullable: false, defaultValue: 10));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "NumberOfRecords");
            DropColumn("dbo.AspNetUsers", "DelfiChannel");
            DropColumn("dbo.AspNetUsers", "Avatar");
        }
    }
}
