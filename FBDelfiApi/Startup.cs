﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FBDelfiApi.Startup))]
namespace FBDelfiApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
